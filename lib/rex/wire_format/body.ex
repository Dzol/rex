defmodule Rex.WireFormat.Body do
  @moduledoc false

  @type t :: Rex.WireFormat.Common.Map.t() | term()

  @spec write(t()) :: binary()
  def write(x) do
    Rex.WireFormat.Common.Map.write(x)
  end

  def encode(x) do
    content = Map.fetch!(x, :content)

    ## These belong w/ the corresponding request somewhere!
    case Map.fetch!(x, :operation) do
      Start ->
        Rex.WireFormat.Common.Map.write(content)

      Query ->
        c = content
        l = Kernel.byte_size(c)

        <<
          l::size(32),
          c::binary,
          0x0005::size(16),
          0x0000::size(8)
        >>
    end
  end

  def decode(Ready, <<>>) do
    nil
  end

  def decode(Result, x) do
    Rex.WireFormat.Body.Result.read(x)
  end

  def decode(Error, x) do
    <<
      code::size(32),
      t::size(16),
      text::binary-size(t),
      _::binary
    >> = x

    %{error: code, reason: text}
  end
end
