defmodule Rex.WireFormat.Header.Stream do
  @moduledoc false

  @type writable :: non_neg_integer()
  @type readable :: non_neg_integer() | -1
  @type t :: readable() | writable()

  @spec read(readable()) :: t()
  def read(x) when is_integer(x) and x >= -1 and x < 32768 do
    x
  end

  @spec write(writable()) :: t()
  def write(x) when is_integer(x) and x >= 0 and x < 32768 do
    x
  end
end
