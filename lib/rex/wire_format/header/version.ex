defmodule Rex.WireFormat.Header.Version do
  @moduledoc false

  @type t :: 4

  @spec read(binary()) :: t()
  def read(<<1::size(1)-unit(1), x::size(7)-unit(1)>>) do
    x
  end

  @spec write(t()) :: binary()
  def write(x) do
    <<0::size(1), x::size(7)>>
  end
end
