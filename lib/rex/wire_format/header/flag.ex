defmodule Rex.WireFormat.Header.FlagBitMap do
  @moduledoc false
end

defmodule Rex.WireFormat.Header.Flag do
  @moduledoc false

  defmodule Mask do
    @moduledoc false

    @type t :: :compression? | :tracing? | :custom? | :warning? | :beta?

    @spec decode(non_neg_integer()) :: t()
    def decode(0x01), do: :compression?
    def decode(0x02), do: :tracing?
    def decode(0x04), do: :custom?
    def decode(0x08), do: :warning?
    def decode(0x10), do: :beta?

    @spec encode(t()) :: non_neg_integer()
    def encode(:compression?), do: 0x01
    def encode(:tracing?), do: 0x02
    def encode(:custom?), do: 0x04
    def encode(:warning?), do: 0x08
    def encode(:beta?), do: 0x10
  end

  @type t :: %{Mask.t() => boolean()}

  @spec write() :: non_neg_integer()
  def write do
    write(default())
  end

  @spec read(non_neg_integer()) :: t()
  def read(0x00) do
    default()
  end

  # def read(x) do
  #   fn power, flag ->
  #     import Bitwise

  #     position = Kernel.round(:math.pow(2, power))
  #     if (x &&& position) > 0 do
  #       name = decode(x &&& position)
  #       Map.put(flag, name, true)
  #     else
  #       flag ## what about defaults?
  #     end
  #   end
  #   Enum.reduce(0, %{}, )
  # end

  @spec write(t()) :: non_neg_integer()
  defp write(x) do
    x
    |> Enum.map(&numeral/1)
    |> Enum.reduce(0, &Kernel.+/2)
  end

  @spec numeral({Mask.t(), boolean}) :: non_neg_integer()
  defp numeral(x) do
    if Kernel.elem(x, 1) do
      Mask.encode(Kernel.elem(x, 0))
    else
      0x00
    end
  end

  @spec default() :: t()
  defp default do
    %{
      compression?: false,
      tracing?: false,
      custom?: false,
      warning?: false,
      beta?: false
    }
  end
end
