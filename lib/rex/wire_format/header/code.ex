defmodule Rex.WireFormat.Header.Code do
  @moduledoc false

  @type t :: atom()

  @spec read(non_neg_integer()) :: t()
  def read(0x00), do: Error
  def read(0x02), do: Ready
  def read(0x08), do: Result
  def read(0x0C), do: Event

  @spec write(t()) :: non_neg_integer()
  def write(Start), do: 0x01
  def write(Query), do: 0x07
  def write(Register), do: 0x0B
end
