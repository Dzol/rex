defmodule Rex.WireFormat.Header.Length do
  @moduledoc false

  @type t :: non_neg_integer()

  @spec read(non_neg_integer()) :: t()
  def read(x) when is_integer(x) and x <= 256_000_000 do
    x
  end

  @spec write(t()) :: non_neg_integer()
  def write(x) when is_integer(x) and x <= 256_000_000 do
    x
  end
end
