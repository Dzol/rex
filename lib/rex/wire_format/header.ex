defmodule Rex.WireFormat.Header do
  @moduledoc false

  alias Rex.WireFormat.Header

  alias Header.Version
  alias Header.Flag
  alias Header.Stream
  alias Header.Code
  alias Header.Length

  @type t :: %__MODULE__{
          version: Version.t(),
          flag: Flag.t(),
          stream: Stream.t(),
          operation: Code.t(),
          length: Length.t()
        }

  defstruct [
    :version,
    :flag,
    :stream,
    :operation,
    :length
  ]

  @spec read(binary()) :: t()
  def read(x) do
    <<
      v::binary-size(1),
      f::size(1)-unit(8),
      s::size(2)-unit(8),
      c::size(1)-unit(8),
      l::size(4)-unit(8)
    >> = x

    %__MODULE__{
      version: Version.read(v),
      flag: Flag.read(f),
      stream: Stream.read(s),
      operation: Code.read(c),
      length: Length.read(l)
    }
  end

  @spec write(t()) :: binary()
  def write(x) do
    v = Version.write(x.version)
    f = Flag.write()
    s = Stream.write(x.stream)
    c = Code.write(x.operation)
    l = Length.write(x.length)

    <<
      v::binary,
      f::size(1)-unit(8),
      s::size(2)-unit(8),
      c::size(1)-unit(8),
      l::size(4)-unit(8)
    >>
  end
end
