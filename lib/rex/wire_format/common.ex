defmodule Rex.WireFormat.Common do
  @moduledoc false

  def integer(<<x::signed-integer-size(4)-unit(8), y::binary>>) do
    [x | y]
  end

  def long(<<x::signed-integer-size(8)-unit(8), y::binary>>) do
    [x | y]
  end

  def short(<<x::integer-size(8)-unit(8), y::binary>>) do
    [x | y]
  end

  def string(<<x::integer-size(8)-unit(8), y::binary-size(x), z::binary>>) do
    [y | z]
  end

  def string(x) do
    [a | b] = short(x)
    <<u::binary-size(a), v::binary>> = b
    [u | v]
  end
end
