defmodule Rex.WireFormat.Body.Data.IPAddress do
  defmodule IV do
    @type t :: :inet.ip4_address()
  end

  defmodule VI do
    @type t :: :inet.ip6_address()
  end

  def decode(<<a, b, c, d>>) do
    {a, b, c, d}
  end
end
