defmodule Rex.WireFormat.Body.Data.TinyInteger do
  def decode(<<x::signed-size(1)-unit(8)>>) do
    x
  end
end
