defmodule Rex.WireFormat.Body.Data.RexSet do
  def decode(<<s::signed-size(4)-unit(8), r::binary>>) do
    decode(r, s)
  end

  defp decode(<<>>, 0) do
    []
  end

  defp decode(<<s::signed-size(4)-unit(8), r::binary>>, x) when s < 0 do
    [nil | decode(r, x - 1)]
  end

  defp decode(<<s::signed-size(4)-unit(8), d::binary-size(s), r::binary>>, x) when s >= 0 do
    [d | decode(r, x - 1)]
  end
end
