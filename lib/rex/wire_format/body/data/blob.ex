defmodule Rex.WireFormat.Body.Data.BLOB do
  @type t :: binary()

  def decode(x) do
    x
  end
end
