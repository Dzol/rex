defmodule Rex.WireFormat.Body.Data.Integer do
  def decode(<<x::signed-size(4)-unit(8)>>) do
    x
  end
end
