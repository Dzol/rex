defmodule Rex.WireFormat.Body.Data.Boolean do
  @type t :: boolean()

  @spec decode(binary()) :: t()
  def decode(<<0>>), do: false
  def decode(<<_>>), do: true

  ## Keep as integer so less binary flying around
  @spec encode(boolean()) :: 0 | 1
  def encode(false), do: 0
  def encode(true), do: 1
end
