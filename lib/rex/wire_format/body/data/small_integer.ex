defmodule Rex.WireFormat.Body.Data.SmallInteger do
  def decode(<<x::signed-size(2)-unit(8)>>) do
    x
  end
end
