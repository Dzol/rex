defmodule Rex.WireFormat.Body.Data do
  @type t :: atom()

  @spec read(non_neg_integer()) :: t()
  def read(0x0000), do: __MODULE__.Custom
  def read(0x0001), do: __MODULE__.ASCII
  def read(0x0002), do: __MODULE__.BigInteger
  def read(0x0003), do: __MODULE__.BLOB
  def read(0x0004), do: __MODULE__.Boolean
  def read(0x0005), do: __MODULE__.Counter
  def read(0x0006), do: __MODULE__.Decimal
  def read(0x0007), do: __MODULE__.Double
  def read(0x0008), do: __MODULE__.Float
  def read(0x0009), do: __MODULE__.Integer
  def read(0x000B), do: __MODULE__.Stamp
  def read(0x000C), do: __MODULE__.UUID
  def read(0x000D), do: __MODULE__.VariableCharacter
  def read(0x000E), do: __MODULE__.VariableInteger
  def read(0x000F), do: __MODULE__.TimeUUID
  def read(0x0010), do: __MODULE__.IPAddress
  def read(0x0011), do: __MODULE__.Date
  def read(0x0012), do: __MODULE__.Time
  def read(0x0013), do: __MODULE__.SmallInteger
  def read(0x0014), do: __MODULE__.TinyInteger
  def read(0x0020), do: __MODULE__.List
  def read(0x0021), do: __MODULE__.Map
  def read(0x0022), do: __MODULE__.Set
  def read(0x0030), do: __MODULE__.UDT
  def read(0x0031), do: __MODULE__.Tuple

  defmodule Container do
    def read(r, t) do
      alias Rex.WireFormat.Body.Data

      case Data.read(t) do
        Data.List ->
          <<s::unsigned-size(2)-unit(8), r::binary>> = r
          r

        Data.Map ->
          <<_key::unsigned-size(2)-unit(8), _value::unsigned-size(2)-unit(8), r::binary>> = r
          r

        Data.Set ->
          <<s::unsigned-size(2)-unit(8), r::binary>> = r

          container = [
            Data.List,
            Data.Map,
            Data.Set,
            Data.UDT,
            Data.Tuple
          ]

          false = Data.read(s) in container
          r

        complex when complex in [Data.UDT, Data.Tuple, Data.Custom] ->
          raise "Oops!"

        _ ->
          r
      end
    end
  end
end
