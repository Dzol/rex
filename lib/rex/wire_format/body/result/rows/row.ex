defmodule Rex.WireFormat.Body.Result.Rows.Cell do
  @moduledoc false

  @type t :: binary()

  @spec decode(map()) :: map()
  def decode(x) do
    x
    |> Map.put(:data, [])
    |> decode(x[:width] * x[:height])
  end

  @spec decode(map(), non_neg_integer()) :: map()
  defp decode(x, 0) do
    Map.update!(x, :data, &Enum.reverse/1)
  end

  defp decode(x, count) when count > 0 do
    case Map.fetch!(x, :rest) do
      <<s::signed-size(4)-unit(8), d::binary-size(s), r::binary>> when s >= 0 ->
        x
        |> Map.replace!(:rest, r)
        |> Map.update!(:data, &[d | &1])
        |> decode(count - 1)

      <<s::signed-size(4)-unit(8), r::binary>> when s < 0 ->
        x
        |> Map.replace!(:rest, r)
        |> Map.update!(:data, &[nil | &1])
        |> decode(count - 1)
    end
  end
end
