defmodule Rex.WireFormat.Body.Result.Rows.ColumnInformation do
  @moduledoc false

  @type t :: %{name: String.t(), type: Rex.WireFormat.Body.Data.t()}

  @spec decode(map()) :: map()
  def decode(x) do
    x
    |> Map.put(:model, [])
    |> decode(x[:width])
  end

  @spec decode(map(), non_neg_integer()) :: map()
  defp decode(x, 0) do
    Map.update!(x, :model, &Enum.reverse/1)
  end

  defp decode(x, count) when count > 0 do
    <<
      s::unsigned-size(2)-unit(8),
      n::binary-size(s),
      t::unsigned-size(2)-unit(8),
      r::binary
    >> = x[:rest]

    r = Rex.WireFormat.Body.Data.Container.read(r, t)

    d = %{name: n, type: Rex.WireFormat.Body.Data.read(t)}

    x
    |> Map.replace!(:rest, r)
    |> Map.update!(:model, &[d | &1])
    |> decode(decrement(count))
  end

  @spec decrement(pos_integer()) :: non_neg_integer()
  defp decrement(x) do
    x - 1
  end
end
