defmodule Rex.WireFormat.Body.Result.Rows.TableInformation do
  @moduledoc false

  @type t :: [space: String.t(), table: String.t()]

  @spec decode(map()) :: map()
  def decode(x) do
    use Bitwise

    ## accumulate the flag integer and binary to read and write
    if (x[:flag] &&& 0x0001) > 0 do
      <<
        s::size(2)-unit(8),
        space::binary-size(s),
        t::size(2)-unit(8),
        table::binary-size(t),
        more::binary
      >> = x[:rest]

      x
      |> Map.replace!(:rest, more)
      |> Map.put(:information, space: space, table: table)
    end
  end
end
