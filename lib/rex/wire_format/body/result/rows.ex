## Page
defmodule Rex.WireFormat.Body.Result.Rows do
  @moduledoc false

  ## Rex.WireFormat.Body.Result.Table
  alias Rex.WireFormat.Body.Result.Rows.TableInformation
  alias Rex.WireFormat.Body.Result.Rows.ColumnInformation
  alias Rex.WireFormat.Body.Result.Rows.Cell

  defmodule Table do
    @moduledoc false

    @type t :: %__MODULE__{
            width: non_neg_integer(),
            height: non_neg_integer(),
            information: TableInformation.t(),
            model: list(ColumnInformation.t()),
            data: list(Cell.t())
          }

    defstruct width: 0,
              height: 0,
              information: [],
              model: [],
              data: []
  end

  @spec read(binary()) :: Table.t()
  def read(x) do
    %{}
    |> Map.put(:rest, x)
    |> flag()
    |> width()
    |> TableInformation.decode()
    |> ColumnInformation.decode()
    |> height()
    |> Cell.decode()
    |> Map.delete(:rest)
    |> parse()
    |> structify(Table)
  end

  @spec flag(map()) :: map()
  defp flag(x) do
    <<f::signed-size(4)-unit(8), r::binary>> = x[:rest]

    x
    |> Map.replace!(:rest, r)
    |> Map.put(:flag, f)
  end

  @spec width(map()) :: map()
  defp width(x) do
    <<w::signed-size(4)-unit(8), r::binary>> = x[:rest]

    x
    |> Map.replace!(:rest, r)
    |> Map.put(:width, w)
  end

  @spec height(map()) :: map()
  defp height(x) do
    <<h::signed-size(4)-unit(8), r::binary>> = x[:rest]

    x
    |> Map.replace!(:rest, r)
    |> Map.put(:height, h)
  end

  @spec parse(map()) :: map()
  defp parse(x) do
    f = fn d ->
      for row <- Enum.chunk_every(d, x[:width]) do
        row
        |> Enum.zip(x[:model])
        |> Enum.map(&transform/1)
      end
    end

    Map.update!(x, :data, f)
  end

  @spec structify(map(), atom()) :: struct()
  defp structify(x, y) do
    Kernel.struct(y, x)
  end

  @spec transform({binary(), %{required(:type) => atom()}}) :: term()
  defp transform({data, %{type: type}}) do
    alias Rex.WireFormat.Body.Data

    known = [
      Data.Set,
      Data.IPAddress,
      Data.VariableCharacter,
      Data.Integer,
      Data.UUID
    ]

    if type in known do
      Kernel.apply(type, :decode, [data])
    else
      data
    end
  end
end
