defmodule Rex.WireFormat.Body.Result.Void do
  @moduledoc false

  @type t() :: nil

  @spec read(binary()) :: nil
  def read(<<>>) do
  end
end
