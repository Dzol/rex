defmodule Rex.WireFormat.Body.Result do
  @moduledoc false

  alias Rex.WireFormat.Body.Result.Void
  alias Rex.WireFormat.Body.Result.Rows

  @type t :: Void.t() | Rows.Table.t()

  @spec read(binary()) :: t()
  def read(<<0x0001::size(32), rest::binary>>) do
    Rex.WireFormat.Body.Result.Void.read(rest)
  end

  def read(<<0x0002::size(32), rest::binary>>) do
    Rex.WireFormat.Body.Result.Rows.read(rest)
  end
end
