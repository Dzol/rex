defmodule Rex.WireFormat.Common.Map do
  @moduledoc false

  @type t :: list(Rex.WireFormat.Common.Map.Entry.t())

  @spec read(binary()) :: t()
  def read(<<>>) do
    []
  end

  def read(<<n::integer-size(2)-unit(8), x::binary>>) do
    Rex.WireFormat.Common.Map.Entry.read(x, n)
  end

  @spec write(t()) :: binary()
  def write(x) do
    l = Kernel.length(x)
    x = Enum.join(Enum.map(x, &Rex.WireFormat.Common.Map.Entry.write/1))
    <<l::size(16), x::binary>>
  end
end
