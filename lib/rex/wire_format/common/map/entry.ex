defmodule Rex.WireFormat.Common.Map.Entry do
  defmodule Name do
    @type t :: String.t()
  end

  defmodule Value do
    @type t :: String.t()
  end

  @type t :: {Name.t(), Value.t()}

  @spec read(binary(), integer()) :: list(t())
  def read(<<>>, 0) do
    []
  end

  # def read(_, 0) do
  #   []
  # end

  def read(x, n) when is_integer(n) and n > 0 do
    <<
      a::integer-size(2)-unit(8),
      name::binary-size(a),
      b::integer-size(2)-unit(8),
      value::binary-size(b),
      more::binary
    >> = x

    [{name, value} | read(more, decrement(n))]
  end

  @spec write(t()) :: binary()
  def write(x) do
    <<
      Kernel.byte_size(name(x))::size(16),
      name(x)::binary,
      Kernel.byte_size(value(x))::size(16),
      value(x)::binary
    >>
  end

  @spec name(t()) :: Name.t()
  defp name(x) do
    Kernel.elem(x, 0)
  end

  @spec value(t()) :: Value.t()
  defp value(x) do
    Kernel.elem(x, 1)
  end

  @spec decrement(pos_integer()) :: non_neg_integer()
  defp decrement(n) do
    n - 1
  end
end
