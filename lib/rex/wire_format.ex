defmodule Rex.WireFormat do
  @moduledoc false

  ## `Rex.WireFormat.Encodable` or `Rex.Protocol.WireFormat.Encodable` implementation
  alias Rex.WireFormat.Header
  ## `Rex.WireFormat.Encodable` or `Rex.Protocol.WireFormat.Encodable` implementation
  alias Rex.WireFormat.Body

  @type neglect() :: term()

  @type t :: %__MODULE__{
          ## certainly opaque
          version: integer(),
          ## perhaps opaque
          flag: Header.Flag.t(),
          ## certainly opaque
          stream: integer(),
          operation: atom(),
          content: map() | list() | String.t()
        }

  defstruct version: 4,
            flag: %{},
            stream: 0,
            operation: nil,
            content: nil

  @spec write(t()) :: binary()
  def write(x) do
    body = Body.encode(x)

    length = Kernel.byte_size(body)

    x = %Header{
      version: 4,
      flag: %{},
      stream: x.stream,
      operation: x.operation,
      length: length
    }

    header = Header.write(x)

    <<header::binary, body::binary>>
  end

  @spec read(binary()) :: t() | neglect()
  def read(x) do
    <<
      header::binary-size(9),
      rest::binary
    >> = x

    h = Header.read(header)

    case Body.decode(h.operation, rest) do
      ## Only here until we merge Body w/ Header into a
      ## Protocol.Response as this is what the test suite wants
      nil ->
        h

      other ->
        other
    end
  end
end
