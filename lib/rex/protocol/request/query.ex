defmodule Rex.Protocol.Request.Query do
  def default do
    %Rex.WireFormat{
      stream: 1,
      operation: Query,
      content: "SELECT * FROM system.local;"
    }
  end
end
