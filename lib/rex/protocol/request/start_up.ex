defmodule Rex.Protocol.Request.StartUp do
  def default do
    option = {"CQL_VERSION", "4.0.0"}

    %Rex.WireFormat{
      stream: 0,
      operation: Start,
      content: [option]
    }
  end
end
