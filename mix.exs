defmodule Rex.MixProject do
  use Mix.Project

  def project do
    [
      app: :rex,
      version: "0.1.0",
      elixir: "~> 1.6",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ] ++ aliases() ++ dialyzer()
  end

  def application do
    [
      extra_applications: [:logger]
    ]
  end

  defp deps do
    [
      {:dialyxir, "~> 1.0.0-rc.2", only: [:dev], runtime: false}
    ]
  end

  defp aliases do
    [aliases: [initialize: &initialize/1]]
  end

  defp initialize(_) do
    unless File.exists?(".dialyzer") do
      File.mkdir!(".dialyzer")
    end
  end

  defp dialyzer do
    [dialyzer: [plt_core_path: ".dialyzer/"]]
  end
end
