defmodule RexTest do
  use ExUnit.Case

  test "START" do
    alias :gen_tcp, as: TCP
    {:ok, s} = TCP.connect(host(), 9042, [:binary, packet: 0] ++ passive())
    :ok = TCP.send(s, Rex.WireFormat.write(Rex.Protocol.Request.StartUp.default()))
    {:ok, r} = TCP.recv(s, 0)

    assert %Rex.WireFormat.Header{
             version: 4,
             stream: 0,
             operation: Ready,
             length: 0
           } = Rex.WireFormat.read(r)

    TCP.close(s)
  end

  test "QUERY" do
    alias :gen_tcp, as: TCP
    ## packet: :raw
    {:ok, s} = TCP.connect(host(), 9042, [:binary, packet: 0] ++ passive())
    :ok = TCP.send(s, Rex.WireFormat.write(Rex.Protocol.Request.StartUp.default()))
    {:ok, r} = TCP.recv(s, 0)

    assert %Rex.WireFormat.Header{
             version: 4,
             stream: 0,
             operation: Ready,
             length: 0
           } = Rex.WireFormat.read(r)

    :ok = TCP.send(s, Rex.WireFormat.write(Rex.Protocol.Request.Query.default()))

    ## TCP.recv(s, Rex.WireFormat.Header.length) assume header always there @ 9 bytes
    r =
      for _ <- 1..5, {:ok, fragment} = TCP.recv(s, 0), into: <<>> do
        fragment
      end

    %Rex.WireFormat.Body.Result.Rows.Table{
      data: data,
      height: 1,
      information: [space: "system", table: "local"],
      model: model,
      width: 18
    } = Rex.WireFormat.read(r)

    assert [
             [
               "local",
               "COMPLETED",
               {_, _, _, _},
               "Test Cluster",
               "3.4.4",
               "datacenter1",
               i,
               h,
               {_, _, _, _},
               "4",
               "org.apache.cassandra.dht.Murmur3Partitioner",
               "rack1",
               "3.11.3",
               {0, 0, 0, 0},
               "ea63e099-37c5-3d7b-9ace-32f4c833653d",
               "20.1.0",
               b,
               nil
             ]
           ] = data

    assert is_integer(i)

    assert length(String.split(h, "-")) === 5

    Enum.all?(b, &assert(String.to_integer(&1) < Kernel.round(:math.pow(10, 7 * 3))))

    assert [
             %{name: "key", type: Rex.WireFormat.Body.Data.VariableCharacter},
             %{name: "bootstrapped", type: Rex.WireFormat.Body.Data.VariableCharacter},
             %{name: "broadcast_address", type: Rex.WireFormat.Body.Data.IPAddress},
             %{name: "cluster_name", type: Rex.WireFormat.Body.Data.VariableCharacter},
             %{name: "cql_version", type: Rex.WireFormat.Body.Data.VariableCharacter},
             %{name: "data_center", type: Rex.WireFormat.Body.Data.VariableCharacter},
             %{name: "gossip_generation", type: Rex.WireFormat.Body.Data.Integer},
             %{name: "host_id", type: Rex.WireFormat.Body.Data.UUID},
             %{name: "listen_address", type: Rex.WireFormat.Body.Data.IPAddress},
             %{name: "native_protocol_version", type: Rex.WireFormat.Body.Data.VariableCharacter},
             %{name: "partitioner", type: Rex.WireFormat.Body.Data.VariableCharacter},
             %{name: "rack", type: Rex.WireFormat.Body.Data.VariableCharacter},
             %{name: "release_version", type: Rex.WireFormat.Body.Data.VariableCharacter},
             %{name: "rpc_address", type: Rex.WireFormat.Body.Data.IPAddress},
             %{name: "schema_version", type: Rex.WireFormat.Body.Data.UUID},
             %{name: "thrift_version", type: Rex.WireFormat.Body.Data.VariableCharacter},
             %{name: "tokens", type: Rex.WireFormat.Body.Data.Set},
             %{name: "truncated_at", type: Rex.WireFormat.Body.Data.Map}
           ] = model

    TCP.close(s)
  end

  defp passive do
    [{:active, false}]
  end

  defp host do
    if System.get_env("ENVIRONMENT") == "CI" do
      'cassandra'
    else
      'localhost'
    end
  end
end
